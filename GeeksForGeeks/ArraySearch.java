class ArraySearch {
    

    static int searchArr (int a[], int n, int key){
        for (int i = 0; i < n; i++){
            if(a[i] == key)
                return i;
        }
  
        return - 1;

    }


    public static void main(String[] args) {
        int a[] = {1,2,3,4,5,6};
        int key = 2;
        int n = a.length;

        int posn = searchArr(a, n, key);

        if(posn == - 1)
            System.out.println("Not Found");
        else
            System.out.println(key +" found at " + (posn + 1) + " position");
        
    }

}
