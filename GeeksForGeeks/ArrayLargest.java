
public class ArrayLargest {
    public long largest(long arr[], long n) {
    	long l = arr[0];
    	for(int i = 0; i < n; i++) {
    		if (l < arr[i]) {
    			l = arr[i];
    		}
    	}
    	return l;
    }
}