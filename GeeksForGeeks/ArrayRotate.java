public class ArrayRotate{
    
    static void reverse(int a[], int low, int high){
        while(low < high){
            int temp = a[low];
            a[low] = a[high];
            a[high] = temp;
            low++;high--;
        }
    }
    static void rotate(int a[],int n,int d){
        reverse(a,0,d-1);
        reverse(a,d,n-1);
        reverse(a,0,n-1);
    }
    
    public static void main (String[] args) {
    	int[] a = {1, 3, 7, 4};
    	reverse(a, 0, 3);
    	for(int i = 0; i < 4; i++) {
    	  	System.out.println(a[i]);  		
    	}
  
    }
}