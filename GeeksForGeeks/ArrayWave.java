public class ArrayWave{
	
	static void waveIt(int[] a, int n) {
		for(int i=0; i<n-1; i+=2) {
			swap(a,i,i+1);
		}
	}
	
	static void swap(int[] a, int x, int y) {
		int temp = a[x];
		a[x] = a[y];
		a[y] = temp;
	}
	
	public static void main(String[] args) {
	int[] a = { 1, 2, 3, 4, 5 };
	waveIt(a,a.length);
	for(int i : a)
		System.out.print(i + " ");
	}
}