public class ArrayBinarySearch {
    

    static int binarySearch(int [] a, int low, int high, int key){

        int mid = (high + low)/2;

        if (key == a[mid])
            return mid;
        if (key > a[mid])
            return binarySearch(a, mid + 1, high, key);
        return binarySearch(a, low, mid-1, key);
    }


    public static void main(String[] args){
        int[] a = { 1, 2, 3, 6, 8, 10, 29 };
        int find = 29;

        int posn = binarySearch(a, 0, a.length, find);

        if(posn == -1){
            System.out.println("Not Found");
        }
        else{
            System.out.println("Found at " + (posn+1));
        }

    }
}