
public class ArrayAscending {
	
	static void ascend(int a[], int n) {
		int temp;
		for (int i = 0; i < n; i++) {
			for(int j = i+1; j<n; j++) {
				if(a[i] > a[j]) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int[] a = { 3, 1, 6, 33, 2, 0, 1 };
		ascend(a, a.length);
		for(int i = 0; i < a.length; i++) {
			System.out.println(a[i]);			
		}
	}
}	