public class ArrayOccurences{
	
	
	static void freqUtil(int a[], int low, int high, int freq[]) {
		if(a[low] == a[high]) {
			freq[a[low]] += high - low + 1;
		}
		else {
			int mid = (low + high)/2;
			freqUtil(a, low, mid, freq);
			freqUtil(a, mid+1, high, freq);
		}
	}
	
	static void freqFind(int a[], int n)
    {
    	
        int[] freq = new int [a[n-1] + 1];
        freqUtil(a, 0, n-1, freq);
        for (int i = 0; i <= a[n - 1]; i++)
            if (freq[i] != 0)
                System.out.println("Element " + i + " occurs " + freq[i] + " times");
    }
    
    public static void main(String[] args) {
    	int[] a = { 1, 1, 2, 3, 4, 5, 5, 5};
    	int n = a.length;
    	
    	freqFind(a, n);
    }
}