
public class ArrayReverse {
	
	static void reverse(int a[], int n){
		for(int i = n - 1; i >= 0; i--) {
			System.out.println(a[i]);
		}
	}
	
	public static void main(String[] args) {
		int[] a = { 1, 2, 3, 4, 5 };
		reverse(a,5);
	}
}
